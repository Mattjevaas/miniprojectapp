import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    View,
    Text,
    StatusBar,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'

import SplashScreen from '../screens/SplashScreen'
import Intro from '../screens/Intro'
import Login from '../screens/Login'
import Main from '../screens/MainScreen'

const Stack = createStackNavigator()

const MainNavigation = () => (

    <Stack.Navigator>
        <Stack.Screen name="Intro" component={Intro} options={{headerShown: false}}/>
        <Stack.Screen name="Login" component={Login} options={{headerShown: false}}/>
        <Stack.Screen name="Main" component={Main} options={{headerShown: false}}/>
    </Stack.Navigator>

)

function AppNavigation(){

    const [isLoading, setLoading] = useState(true)

    useEffect(()=>{
        setTimeout(()=>{
            setLoading(!isLoading)
        },3000)
    },[])

    if(isLoading) {
        return (<SplashScreen/>)
    }

    return (
        <NavigationContainer>
            <MainNavigation/>
        </NavigationContainer>
    );

};

const styles = StyleSheet.create({

});

export default AppNavigation;
