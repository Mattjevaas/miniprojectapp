import React from 'react';
import { View, Text, Image, StatusBar, StyleSheet } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider'; 
import Icon from 'react-native-vector-icons/Ionicons';

const slides = [
    {
        key: 1,
        title: 'Mudah Digunakan',
        text: 'Tinggal login, gunakan aplikasi sepuasmu',
        image: require('../../assets/images/logo.jpg')
    },
    {
        key: 2,
        title: 'Dapat kenalan Baru',
        text: 'Cari kenalan baru dan pengalaman baru yang menarik',
        image: require('../../assets/images/logo.jpg'),
    },
    {
        key: 3,
        title: 'Capture Momen Terbaikmu',
        text: 'Bagikan momen terbaikmu ke semua orang',
        image: require('../../assets/images/logo.jpg'),
    },
];

const Intro = ({ navigation }) => {

    const renderItem = ({ item }) => {
        return (
            <View style={styles.slide}>
                <Text style={styles.title}>{item.title}</Text>
                <Image source={item.image} style={styles.image} />
                <Text style={styles.text}>{item.text}</Text>
            </View>
        );
    }

    const onDone = () => {
        navigation.navigate('Login')
    }

    const renderDoneButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Icon
                    name="md-checkmark"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };

    const renderNextButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Icon
                    name="arrow-forward"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };

    return (

        <View style={styles.sliderContainer}>
            <AppIntroSlider
                data={slides}
                onDone={onDone}
                renderItem={renderItem}
                renderDoneButton={renderDoneButton}
                renderNextButton={renderNextButton}
                keyExtractor={(item, index) => index.toString()}
                activeDotStyle={{ backgroundColor: '#3d84a8' }}
            />
        </View>

    )
}

const styles = StyleSheet.create({
    sliderContainer: {
        flex: 1,
        backgroundColor: 'white',
    },
    slide:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonCircle: {
        backgroundColor: '#48466d',
        width: 50,
        height: 50,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        textAlign: 'center',
        fontWeight: 'bold',
        color: 'grey'
    },
    title: {
        fontSize: 22,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#3d84a8'
    },
    image: {
        marginVertical: 50,
    }


})

Icon.loadFont();
export default Intro