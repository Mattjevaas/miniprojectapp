import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
    Modal,
    FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {GoogleSignin} from '@react-native-community/google-signin'
import AsyncStorage from '@react-native-community/async-storage'
import { RNCamera } from 'react-native-camera'; 
import storage from '@react-native-firebase/storage';

function MainScreen({navigation}){

    const [userData, setUserData] = useState(null)
    const [isVisible, setVisible] = useState(false)
    const [isVisibleTwo, setVisibleTwo] = useState(false)
    const [isVisibleThree, setVisibleThree] = useState(false)
    const [state, setState] = useState(RNCamera.Constants.Type.back)
    const [photo,setPhoto] = useState(null)
    const [imagesUrl, setImagesUrl] = useState([])
    const [image, setImage] = useState(null)
    const reference = storage().ref('images');

    useEffect(()=>{
        getUserData()
        getImages()
    },[])

    const getUserData = async () => {

        try{
            let userInfo = await GoogleSignin.getCurrentUser();
            setUserData(userInfo)
        }catch(err){
            alert(err)
        }
    }

    const getImages = async () => {
        setImagesUrl([])
        await listFilesAndDirectories(reference).then(() => {
            console.log("Success Fetch Images");
        });
    }

    const onClickLogout = async () => {
        try{

            let value = await AsyncStorage.getItem("token");
            let userInfo = await GoogleSignin.getCurrentUser();

            if(value && userInfo){
                await AsyncStorage.removeItem("token")
                await GoogleSignin.revokeAccess()
                await GoogleSignin.signOut()
            }
            else if(value){
                await AsyncStorage.removeItem("token")
            }
            else if(userInfo){
                await GoogleSignin.revokeAccess()
                await GoogleSignin.signOut()
            }
            
            navigation.navigate('Login')
        }catch(err){
            alert(err)
        }
    }

    const toggleCamera = () => {
        setState(state === RNCamera.Constants.Type.back ? RNCamera.Constants.Type.front : RNCamera.Constants.Type.back)
    }

    const takePicture = async() => {
        const options = {quality: 0.5, base64: true}

        if(camera){
            const data = await camera.takePictureAsync(options)
            await setPhoto(data)
            setVisible(false)
            setVisibleTwo(true)
        }
        
    }

    const uploadImage = (uri) => {
        const sessionID = new Date().getTime()

        return storage()
        .ref(`images/${sessionID}`)
        .putFile(uri)
        .then(result => {
            alert('Upload Success!')
            setVisibleTwo(false)
            setPhoto(null)
            getImages()
        })
        .catch(err=> {
            alert(err)
        })
    }

    function listFilesAndDirectories(reference, pageToken) {
        return reference.list({ pageToken }).then(result => {

            result.items.forEach(ref => {
                setImagesUrl( imagesUrl => [...imagesUrl, ref.fullPath])
            });
        
            if (result.nextPageToken) {
                return listFilesAndDirectories(reference, result.nextPageToken);
            }
        
            return Promise.resolve();
        });
    }

    const RenderCamera = () => {

        return (
            <Modal visible={isVisible} onRequestClose={()=>setVisible(false)}>
                <View style={{flex:1}}>
                    <RNCamera
                        style={{flex:1}}
                        ref={ref=>{
                            camera=ref
                        }}
                        type={state}
                    >
                        <View style={styles.toggleContainer}>
                            <TouchableOpacity style={styles.toogleButton} onPress={()=>toggleCamera()}>
                                <Icon name="rotate-3d-variant" size={20}/>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 1}}/>
                        <View style={styles.cameraButtonContainer}>
                            <TouchableOpacity style={styles.cameraButton} onPress={()=>takePicture()}>
                                <Icon name="camera" size={30}/>
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    const ModalUpload = () => {
        return(
            <Modal 
            visible={isVisibleTwo} 
            animationType="slide"
            transparent={true}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.boxRegister}>
                        <Image style={styles.imagePhoto} source={photo ? {uri: photo.uri} : {uri: ''}}/>
                        <TouchableOpacity style={styles.buttonOneRegister} onPress={()=>uploadImage(photo.uri)}>
                            <Text style={{color: 'white'}}>Upload</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonTwoRegister} onPress={()=>{setVisibleTwo(false),setPhoto(null)}}>
                            <Text style={{color: 'white'}}>Batal</Text>
                        </TouchableOpacity>
                    </View>
                </View>
        </Modal>
        )
    }

    const ModalImage = () => {
        return(
            <Modal 
            visible={isVisibleThree} 
            animationType="slide"
            transparent={true}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.boxRegister}>
                        <Image style={styles.imagePhoto} source={image ? {uri: image} : require('../../assets/images/default-img.png')}/>
                        <TouchableOpacity style={[styles.buttonTwoRegister, {margin: 20}]} onPress={()=>{setVisibleThree(false),setImage(null)}}>
                            <Text style={{color: 'white'}}>Close</Text>
                        </TouchableOpacity>
                    </View>
                </View>
        </Modal>
        )
    }

    const openImage = (uri) => {

        return storage().ref(uri).getDownloadURL().then((url)=>{
            setImage(url)
            setVisibleThree(true)
            console.log(image)
        })


    }

    const ImagesComponent = ({item,index}) => {
        return (
            <TouchableOpacity key={index} style={styles.containerImageList} onPress={()=>openImage(item)}>
                <Text style={styles.imagePathText}>{item}</Text>
            </TouchableOpacity>
        )
    }

    return (
        <View style={styles.container}>
            <RenderCamera/>
            <ModalUpload/>
            <ModalImage/>
            <View style={styles.navbar}>
                <TouchableOpacity style={styles.plusButton} onPress={()=>{setVisible(true)}}>
                    <Text style={styles.textButton}>+</Text>
                </TouchableOpacity>
                <Text style={styles.textTitle}>MiniGr@m!</Text>
                <Image style={styles.avaImage} source={ userData ? {uri: userData.user.photo} : require('../../assets/images/default-avatar.png')}/>
            </View>
            <View style={{flex:1}}>
                <Text style={styles.greetingText}>Hello, {userData ? userData.user.givenName : "Unknown"} !</Text>
                <FlatList
                    data={imagesUrl}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={ImagesComponent}
                />
            </View>
            <TouchableOpacity style={styles.buttonLogout} onPress={()=>onClickLogout()}>
                <Text style={styles.buttonTextLogout}>LOGOUT</Text>
            </TouchableOpacity>
        </View>
    );

};

const styles = StyleSheet.create({
    container: {
        flex:1 ,
        backgroundColor: 'white',
    },
    navbar:{
        height: 70,
        backgroundColor: '#48466d',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 10
    },
    plusButton: {
        width: 40,
        height: 40,
        backgroundColor: '#3d84a8',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textButton: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold'
    },
    textTitle: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold'
    },
    avaImage: {
        width: 40,
        height: 40,
        borderRadius: 20
    },
    buttonLogout : {
        height: 40,
        borderRadius: 50,
        backgroundColor: '#3d84a8',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    buttonTextLogout: {
        fontWeight: 'bold',
        fontSize: 15,
        color: 'white'
    },
    greetingText: {
        fontWeight: 'bold',
        fontSize: 20,
        margin: 10
    },
    toogleButton: {
        width: 50,
        height: 50,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'grey',
    },
    toggleContainer: {
        justifyContent: 'center',
        marginTop: 10,
        marginLeft: 20
        
    },
    cameraButtonContainer: {
        justifyContent:'center',
        alignItems: 'center',
        margin: 20
    },
    cameraButton: {
        width: 80,
        height: 80,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'grey',
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonOneRegister : {
        backgroundColor: '#3d84a8',
        justifyContent: 'center',
        alignItems: 'center',
        height: 38,
        borderRadius: 50,
        shadowOffset: {width: 1, height: 1},
        shadowOpacity: 0.5,
        elevation: 5,
        marginVertical: 10,
    },
    boxRegister: {
        backgroundColor: 'white',
        height: 500,
        width: 350,
        borderRadius: 30,
        elevation: 10,
        padding: 20,
    },
    buttonTwoRegister : {
        backgroundColor: '#dd2c00',
        justifyContent: 'center',
        alignItems: 'center',
        height: 38,
        borderRadius: 50,
        shadowOffset: {width: 1, height: 1},
        shadowOpacity: 0.5,
        elevation: 5,
    },
    imagePhoto: {
        height: '80%',
        width: '100%',
        borderRadius: 20,
    },
    containerImageList: {
        borderRadius: 20,
        elevation: 4,
        height: 60,
        margin: 10,
        justifyContent: 'center',
        padding: 10,
        backgroundColor: 'white'
    },
    imagePathText: {
        fontWeight: 'bold',
    }
});

Icon.loadFont()
export default MainScreen;
