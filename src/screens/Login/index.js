import React, {useState, useEffect} from 'react';
import {
    StyleSheet,
    View,
    Text,
    StatusBar,
    Image,
    TextInput,
    TouchableOpacity,
    Modal
} from 'react-native';

import api from '../../api'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import auth from '@react-native-firebase/auth'
import {GoogleSignin, GoogleSigninButton} from '@react-native-community/google-signin'
import TouchID from 'react-native-touch-id'

function Login({navigation}){
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')
    const [isVisible, setVisible] = useState(false)

    useEffect(()=>{
        configureToken()
    },[])

    const onClickLogin = ()=>{
        let data = {
            email: email,
            password: password
        }

        Axios.post(`${api}/login`,data, {
            timeout: 20000
        }).then((res)=>{
            saveToken(res.data.token)
            navigation.navigate('Main')
        }).catch((err)=>{
            alert(err)
        })
    }

    const saveToken = async (token) => {
        try{
            await AsyncStorage.setItem("token",token)
        }catch(err){
            alert(err)
        }
    }

    const configureToken =  async () => {
        try{
            await GoogleSignin.configure({
                offlineAccess: true,
                webClientId: '257300935241-7g4fgbdrmtt8c1pjmualesor02f56jnf.apps.googleusercontent.com'
            })
        }catch(err){
            alert(err)
        }
        
    }

    const googleSignIn = async()=>{
        try{
            const {idToken} = await GoogleSignin.signIn()

            const credential = auth.GoogleAuthProvider.credential(idToken)

            auth().signInWithCredential(credential)
            navigation.navigate('Main')

        }catch(err){
            alert(err)
        }
    }

    const optionalConfigObject = {
        title: 'Authentication Required', // Android
        imageColor: '#e00606', // Android
        imageErrorColor: '#ff0000', // Android
        sensorDescription: 'Touch sensor', // Android
        sensorErrorDescription: 'Failed', // Android
        cancelText: 'Cancel', // Android
    };

    const onClickTouchid = () => {
        TouchID.authenticate('', optionalConfigObject)
            .then( () => {
                navigation.navigate('Main')
            }).catch(error => {
                alert(error.details)
            })
    }

    const ModalRegister = () => {

        const [name,setName] = useState('')
        const [emailNew,setEmailNew] = useState('')
        const [passwordNew,setPasswordNew] = useState('')

        const onClickRegister = () => {
            let data = {
                name: name,
                email: emailNew,
                password: passwordNew,
            }
    
            Axios.post(`${api}/register`,data, {
                timeout: 20000
            }).then((res)=>{
                alert("Berhasil Register !")
                setVisible(false)
            }).catch((err)=>{
                alert(err)
            })
        }

        return(
            <Modal 
            visible={isVisible} 
            animationType="slide"
            transparent={true}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.boxRegister}>
                        <View style={styles.fieldGroup}>
                            <Text style={styles.titleField} >Nama Lengkap</Text>
                            <TextInput 
                                style={styles.textInput} 
                                placeholder="Masukan Nama Lengkap"
                                onChangeText={(text)=>setName(text)}
                            />
                        </View>
                        <View style={styles.fieldGroup}>
                            <Text style={styles.titleField} >Email</Text>
                            <TextInput 
                                style={styles.textInput} 
                                placeholder="Masukan Email"
                                onChangeText={(text)=>setEmailNew(text)}
                            />
                        </View>
                        <View style={styles.fieldGroup}>
                            <Text style={styles.titleField} >Password</Text>
                            <TextInput 
                                style={styles.textInput} 
                                placeholder="Masukan Password"
                                secureTextEntry={true}
                                onChangeText={(text)=>setPasswordNew(text)}
                            />
                        </View>
                        <TouchableOpacity style={styles.buttonOneRegister} onPress={()=>onClickRegister()}>
                            <Text style={{color: 'white'}}>Register</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonTwoRegister} onPress={()=>{setVisible(false)}}>
                            <Text style={{color: 'white'}}>Batal</Text>
                        </TouchableOpacity>
                    </View>
                </View>
        </Modal>
        )
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <ModalRegister/>
            <View style={styles.titleContainer}>
                <Text style={styles.title}>MiniGr@m!</Text>
            </View>
            <View style={styles.fieldContainer}>
                <View style={styles.fieldGroup}>
                    <Text style={styles.titleField} >Username</Text>
                    <TextInput 
                        style={styles.textInput} 
                        placeholder="Username or Email"
                        onChangeText={(text)=>setEmail(text)}
                    />
                </View>
                <View style={styles.fieldGroup}>
                    <Text style={styles.titleField} >Password</Text>
                    <TextInput 
                        style={styles.textInput} 
                        placeholder="Password"
                        secureTextEntry={true}
                        onChangeText={(text)=>setPassword(text)}
                    />
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.buttonOne} onPress={()=>onClickLogin()}>
                        <Text style={{color: 'white'}}>LOGIN</Text>
                    </TouchableOpacity>
                    <View style={styles.separatorContainer}>
                        <View style={styles.separator}/>
                        <Text>OR</Text>
                        <View style={styles.separator}/>
                    </View>
                    <GoogleSigninButton
                        style={{width: '100%', height: 50}}
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Dark}
                        onPress={()=>googleSignIn()}
                    />
                    <TouchableOpacity style={styles.buttonTwo} onPress={()=>onClickTouchid()}>
                        <Text style={{color: 'white'}}>SIGN IN WITH FINGERPRINT</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                    <Text>Belum mempunyai akun ? </Text>
                    <TouchableOpacity onPress={()=>{setVisible(true)}}>
                        <Text style={{color: 'darkblue'}}>Buat Akun</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )

};

const styles = StyleSheet.create({

    container:{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'space-around',
        marginBottom: 10
    },
    fieldContainer: {
        marginHorizontal: 20,
        marginTop: -100
    },
    textInput: {
        borderBottomWidth: 1,
        borderBottomColor: 'grey',
        padding: 5,
        marginVertical: 10
    },
    titleField :{
        fontWeight: 'bold'
    },
    fieldGroup: {
        marginVertical: 10,
    },
    buttonContainer: {
        marginVertical: 20
    }, 
    buttonOne : {
        backgroundColor: '#3d84a8',
        justifyContent: 'center',
        alignItems: 'center',
        height: 38,
        borderRadius: 50,
        shadowOffset: {width: 1, height: 1},
        shadowOpacity: 0.5,
        elevation: 5,

    },
    buttonTwo: {
        marginTop: 10,
        backgroundColor: '#48466d',
        justifyContent: 'center',
        alignItems: 'center',
        height: 38,
        borderRadius: 50,
        shadowOffset: {width: 1, height: 1},
        shadowOpacity: 0.5,
        elevation:5,
    },
    buttonOneRegister : {
        backgroundColor: '#3d84a8',
        justifyContent: 'center',
        alignItems: 'center',
        height: 38,
        borderRadius: 50,
        shadowOffset: {width: 1, height: 1},
        shadowOpacity: 0.5,
        elevation: 5,
        marginVertical: 20,
    },
    buttonTwoRegister : {
        backgroundColor: '#dd2c00',
        justifyContent: 'center',
        alignItems: 'center',
        height: 38,
        borderRadius: 50,
        shadowOffset: {width: 1, height: 1},
        shadowOpacity: 0.5,
        elevation: 5,

    },
    separator: {
        height: 2,
        width: 170,
        backgroundColor: 'grey',
        marginVertical: 40,
        marginHorizontal: 10,
    },
    separatorContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    toastBox: {
        backgroundColor: '#f7bd0f',
        height: 40,
        width: 150,
        borderRadius: 2,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 50,
        fontStyle: 'italic',
        color: '#46cdcf',
        borderBottomColor: '#3d84a8',
        borderBottomWidth: 3
    },
    titleContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    boxRegister: {
        backgroundColor: 'white',
        height: 450,
        width: 350,
        borderRadius: 30,
        elevation: 10,
        padding: 20,
    }

});

export default Login;
