/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  StatusBar,
} from 'react-native';
import Navigation from './src/navigations'
import firebase from '@react-native-firebase/app';

var firebaseConfig = {
  apiKey: "AIzaSyAe1vL91w3JBZMv4TKS3p8pHvgU8SPwIF8",
  authDomain: "miniprojectapp-b651d.firebaseapp.com",
  databaseURL: "https://miniprojectapp-b651d.firebaseio.com",
  projectId: "miniprojectapp-b651d",
  storageBucket: "miniprojectapp-b651d.appspot.com",
  messagingSenderId: "257300935241",
  appId: "1:257300935241:web:6b710254b9a6da9e1bc598",
  measurementId: "G-FFL25DVJCC"
};
// Initialize Firebase
if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

function App(){
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <KeyboardAvoidingView style={{flex: 1}} behavior={Platform.OS == "ios" ? "padding" : "height"} enabled={false} >
        <SafeAreaView style={styles.container}>
          <Navigation/>
        </SafeAreaView>
      </KeyboardAvoidingView>
    </>
  );
};

const styles = StyleSheet.create({

  container: {
    flex: 1
  }

});

export default App;
